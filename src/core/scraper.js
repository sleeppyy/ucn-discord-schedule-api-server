import scraper from "scrape-it"
import config from "config";

async function scrape(url) {
    const { data } = await scraper(url, {
        week: {
            listItem: ".weekDay",
            data: {
                lessons: {
                    listItem: ".bookingDiv",
                    data: {
                        subject: {
                            attr: "title"
                        }
                    }
                }
            }
        }
    })

    data.week = data.week.filter((v, i) => i < 5)
    data.week.forEach(day => {
        let lessons = []
        day.lessons.forEach(v => {
            const day = v.subject.substr(0, 2)
            const month = v.subject.substr(3, 2)
            const year = v.subject.substr(6, 4)
            const startHour = v.subject.substr(11, 2)
            const startMin = v.subject.substr(14, 2)
            const endHour = v.subject.substr(19, 2)
            const endMin = v.subject.substr(22, 2)

            const startDate = new Date()
            startDate.setFullYear(year, month - 1, day)
            startDate.setHours(startHour, startMin)
            const endDate = new Date()
            endDate.setFullYear(year, month - 1, day)
            endDate.setHours(endHour, endMin)
            const content = v.subject.substr(25)
            const split = content.split(",")

            let teacher
            if (split.length > 2) {
                teacher = split[2].trimLeft()
                teacher = teacher.substr(0, teacher.toLowerCase().indexOf("id ") - 1)
            }
            lessons.push({
                subject: split[0].trimLeft(),
                location: split[1].trimLeft(),
                teacher: teacher,
                dates: [startDate, endDate]
            })
        })

        day.lessons = lessons
    })

    return data.week
}

export default scrape
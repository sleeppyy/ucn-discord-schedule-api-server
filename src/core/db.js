import mysql from "mysql2"
import config from "config"

class Database {
	constructor() {
		this.conn = mysql.createConnection({
			host: config.get("mysql.host"),
			user: config.get("mysql.user"),
			password: config.get("mysql.password"),
			database: config.get("mysql.database"),
			debug: config.get("mysql.debug")
		})

		this.conn.connect(err => {
			if (err) {
				throw err
			}

			this.setup()
		})
	}

	setup() {
		this.execute(`
			CREATE TABLE IF NOT EXISTS ucn_schedule_webhooks (
				webhook_id VARCHAR(128) PRIMARY KEY,
				url_id TEXT NOT NULL
			)
		`)

		this.execute(`
			CREATE TABLE IF NOT EXISTS ucn_schedule_latest (
				webhook_id VARCHAR(64) PRIMARY KEY,
				failed INT(11) DEFAULT 0,
				hash CHAR(64) NOT NULL,
				CONSTRAINT fk_ucn_schedule_latest
					FOREIGN KEY (webhook_id) REFERENCES ucn_schedule_webhooks (webhook_id)
					ON DELETE CASCADE
			)
		`)
	}

	execute(sql, options = []) {
		return new Promise((resolve, reject) => {
			this.conn.execute(sql, options, (err, results, fields) => {
				if (err) {
					return reject(err)
				}

				return resolve(results)
			})
		})
	}
}

export default new Database()
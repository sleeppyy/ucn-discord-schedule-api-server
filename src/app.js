import express from "express"
import scraper from "./core/scraper"
import db from "./core/db"
import config from "config"
import axios from "axios"
import moment from "moment"
import shajs from "sha.js"

const app = express()
app.use(express.json())
app.use(express.urlencoded({
    extended: true
}))

app.post("/webhook", async (req, res) => {
    const { webhook, url } = req.body
    if (!webhook || !url) { return res.status(403).send({ err: true, errMsg: "Missing parameters" }) }

    try {
        let webhookSplit = webhook.split("/")
        webhookSplit = `${webhookSplit[5]}/${webhookSplit[6]}`
        let urlSplit = url.split("/")
        urlSplit = urlSplit[6]
        // Strip the extension
        urlSplit = urlSplit.replace(/\.[^/.]+$/, "")

        await db.execute(`
            INSERT INTO ucn_schedule_webhooks (url_id, webhook_id)
            VALUES (?, ?)
        `, [urlSplit, webhookSplit])

        res.send({
            err: false
        })
    } catch(err) {
        let msg = err.toString()
        if (err.code == "ER_DUP_ENTRY") {
            msg = "Duplicate webhook_id"
        }

        res.status(500).send({
            err: true,
            errMsg: msg
        })
    }
})

;
const timezone = "Europe/Copenhagen";
(async () => {
    const webhooks = await db.execute("SELECT * FROM ucn_schedule_webhooks")
    for (const entry of webhooks) {
        const webhookId = entry.webhook_id
        const urlId = entry.url_id
        const url = `https://cloud.timeedit.net/ucn/web/public/${urlId}.html`
        const scrapedData = await scraper(url)
        const firstDate = scrapedData[0].lessons[0].dates[0]
        const week = moment().format("W")
        const year = firstDate.getFullYear()

        let fields = []
        scrapedData.forEach(day => {
            if (day.lessons.length == 0) { return }

            const date = day.lessons[0].dates[0]
            const dayFormatted = moment(date, timezone).format("dddd - DD/MM/YY")

            fields.push({
                // We got an invis character here, U+2800
                name: "⠀",
                value: dayFormatted
            })
            day.lessons.forEach(lesson => {
                const time = moment(lesson.dates[0], timezone).format("HH:mm") + "-" + moment(lesson.dates[1]).format("HH:mm")
                let value = lesson.subject
                if (lesson.teacher) {
                    // Finds all capitalised letters, we can assume that's gonna be the teacher
                    const initials = lesson.teacher.match(/[A-Z]+/g).join("")

                    value += ` [${initials}]`
                }
                fields.push({
                    name: time,
                    value: value,
                    inline: true
                })
            })
        })

        const hash = shajs("sha256").update(JSON.stringify(fields)).digest("hex")
        const is = "";
        
        await axios.post(`https://discordapp.com/api/webhooks/${webhookId}`, {
            embeds: [
                {
                    color: 0x006699,
                    title: `Schedule for week ${week}, ${year}`,
                    url: `https://cloud.timeedit.net/ucn/web/public/${urlId}.html`,
                    fields
                }
            ]
        })

    }
})()

const port = config.get("port")
app.listen(port, () => console.log(`Listening on port ${port}`))